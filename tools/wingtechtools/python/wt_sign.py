#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import commands
import sys
import getpass
import time
import ftplib

sign_server_ip='192.168.7.138'
sign_server_path='sign'
CONST_BUFFER_SIZE=8192
SIGN_MAX_TIME=3600
SIGN_SPLIT_WAIT_TIME=5


def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]
    
def wt_get_date():
    return time.strftime('%Y_%m_%d')

def wt_get_time():
    return time.strftime('%H%M%S')
    
def wt_get_user():
    return getpass.getuser()
    
def find_dir_updir(basepath,dirname):
    find_dir = basepath
    if not os.path.isdir(basepath) or not basepath.endswith('/'):
        print "not exist basepath or basepath must endswith '/' but \nbasepath = %s"%basepath
    while os.path.abspath(find_dir) != '/' and not os.path.exists(find_dir+dirname):
        find_dir=find_dir+"../"
    return os.path.abspath(find_dir)
def local_repo_code_dir():
    basepath=os.getcwd() + "/"
    return find_dir_updir(basepath,".repo")
    
def usage(platform_customs):
    print "\n\n"
    for i in platform_customs:
        for j in os.listdir('android/tools/wingtechtools'):
            if j.startswith('wt_sign_config') and i.startswith('89'):
                print "usage:  ./wt sign %s android/tools/wingtechtools/%s"%(i.strip(),j)
    print "usage:  ./wt sign %s config_file"%i.strip()
    print "\n\n    config_file must contain lines like boot.img:B3I100EUOD000.MBN\n\n you can create a file like android/tools/wingtechtools/python/wt_sign_config"
    sys.exit(-1)

def connect():
    try:
        ftp = ftplib.FTP(sign_server_ip)
        ftp.login('','')
        return ftp
    except Exception:
        print("FTP is unavailable,please check the host,username and password!")
        sys.exit(0)

def disconnect(ftp):
    ftp.quit()

def _upload(ftp, filepath):
    f = open(filepath, "rb")
    file_name = os.path.split(filepath)[-1]
    try:
        ftp.storbinary('STOR %s'%file_name, f, CONST_BUFFER_SIZE)
    except ftplib.error_perm:
        return False
    return True

def _download(ftp,src,dst):
    if find(ftp,src):
        f = open(dst,"wb").write
        try:
            ftp.retrbinary("RETR %s"%src, f, CONST_BUFFER_SIZE)
        except ftplib.error_perm:
            return False
        return True

def find(ftp,file):
    filepath=os.path.dirname(file)
    filename=os.path.basename(file)
    ftp_f_list = ftp.nlst(filepath)
    if file in ftp_f_list:
        return True
    else:
        return False
        
def ftp_upload(LOCAL_WT_CODE_DIR,spec,d,platform_custom):
    ftp = connect()
    for i in d.keys():
        src = LOCAL_WT_CODE_DIR+'/'+i
        dst = spec + '/' + i
        dst_path=os.path.dirname(dst)
        ftp.cwd('/')
        i=0
        while i < len(dst_path.split('/')):
            try:
                ftp.mkd(dst_path.split('/')[i])
            except Exception:
                pass
            ftp.cwd(dst_path.split('/')[i])
            i=i+1
        _upload(ftp,src)
    os.system("touch ok.txt %s"%platform_custom)
    ftp.cwd('/'+spec)
    _upload(ftp,'ok.txt')
    _upload(ftp,platform_custom)
    os.system("rm ok.txt %s"%platform_custom)
    disconnect(ftp)

def ftp_download(LOCAL_WT_CODE_DIR,spec,d):
    ftp = connect()
    for i in d.keys():
        if d[i] != "":
            src = spec + '/' + i
            src_name=os.path.basename(src)
            dst = os.path.abspath(LOCAL_WT_CODE_DIR+'/'+d[i])
            dst_name=os.path.basename(dst)
            dst_path=os.path.dirname(dst)
            print "download file : %s"%dst
            # os.system("cp %s %s_bak ; rm -f %s ; mkdir -p %s"%(dst,dst,dst,dst_path))
            os.system("rm -f %s ; mkdir -p %s"%(dst,dst_path))
            os.chdir(dst_path)
            try:
                ftp.cwd('/'+os.path.dirname(src))
                _download(ftp,src_name,dst_name)
            except Exception:
                print "can not access ftp path : %s"%os.path.dirname(src)
    d_other={
    'venus.b00':'wt_sign_server/venus.b00',
    'venus.b01':'wt_sign_server/venus.b01',
    'venus.b02':'wt_sign_server/venus.b02',
    'venus.b03':'wt_sign_server/venus.b03',
    'venus.b04':'wt_sign_server/venus.b04',
    'venus.b05':'wt_sign_server/venus.b05',
    'venus.b06':'wt_sign_server/venus.b06',
    'venus.mdt':'wt_sign_server/venus.mdt',
    }
    for i in d_other.keys():
        if d_other[i] != "":
            src = spec + '/' + i
            src_name=os.path.basename(src)
            dst = os.path.abspath(LOCAL_WT_CODE_DIR+'/'+d_other[i])
            dst_name=os.path.basename(dst)
            dst_path=os.path.dirname(dst)
            print "download file : %s"%dst
            os.system("cp %s %s_bak ; rm -f %s ; mkdir -p %s"%(dst,dst,dst,dst_path))
            os.chdir(dst_path)
            try:
                ftp.cwd('/'+os.path.dirname(src))
                _download(ftp,src_name,dst_name)
            except Exception:
                print "can not access ftp path : %s"%os.path.dirname(src)
    disconnect(ftp)
    
def wait_for_sign(wait_time,file_flag):
    ftp = connect()
    local_time=0
    while not find(ftp,file_flag) and local_time < wait_time:
        print "wait for sign ... %ss"%local_time
        time.sleep(SIGN_SPLIT_WAIT_TIME)
        local_time=local_time+SIGN_SPLIT_WAIT_TIME
    if local_time >= wait_time:
        print "timeout"
        disconnect(ftp)
        sys.exit(-1)
    print "sign successful ... OK "
    disconnect(ftp)

def upload(LOCAL_WT_CODE_DIR,spec,d,platform_custom):
    ftp_upload(LOCAL_WT_CODE_DIR,spec,d,platform_custom)
    
def download(LOCAL_WT_CODE_DIR,spec,d):
    ftp_download(LOCAL_WT_CODE_DIR,spec,d)
    
def download_platform_customs():
    os.system('rm -rf platform_customs')
    ftp = connect()
    _download(ftp,'unsigned/platform_customs','platform_customs')
    disconnect(ftp)
    if not os.path.isfile('platform_customs'):
        print "download file platform_customs failed , please check"
        sys.exit(-1)
def copy_sth(src,dst):
    if os.path.isdir(src):
        os.system("cp -raf %s %s"%(src,os.path.dirname(dst)))
    elif os.path.isfile(src):
        os.system("cp -raf %s %s"%(src,dst))
    else:
        print "not exists : %s"%src
        # sys.exit(-1)
    
if __name__ == '__main__':
    download_platform_customs()
    f=open('platform_customs','r')
    platform_customs= f.readlines()
    os.system('rm -rf platform_customs')
    
    if not len(sys.argv) == 3:
        usage(platform_customs)
    platform_custom=sys.argv[1]
    if platform_custom+"\n" not in platform_customs:
        usage(platform_customs)
    config_file=sys.argv[2]
    LOCAL_WT_CODE_DIR = local_repo_code_dir()
    
    print "config_file : %s"%config_file
    print "base dir : %s"%LOCAL_WT_CODE_DIR
    
    if LOCAL_WT_CODE_DIR =='/':
        print "not find .repo"
        sys.exit(-1)
        
    if not os.path.isfile(config_file):
        print "not exist config file %s , please check !!! "%config_file
        sys.exit(-1)
        
    f=open(config_file,'r')
    d={}
    build_date=wt_get_date()
    build_user=wt_get_user()
    build_time=wt_get_time()
    for line in f:
        if len(line.split(':')) == 2:
            src=line.split(':')[0].strip()
            dst=line.split(':')[1].strip()
            d[src]=dst
    f.close()
    
    print d
    spec_unsigned = 'unsigned/' + build_date + '/' + build_user + '/' + build_time
    upload(LOCAL_WT_CODE_DIR,spec_unsigned,d,platform_custom)
    
    spec_signed = 'signed/' + build_date + '/' + build_user + '/' + build_time
    flag_file=spec_signed+'/ok.txt'
    wait_for_sign(SIGN_MAX_TIME,flag_file)
    
    download(LOCAL_WT_CODE_DIR,spec_signed,d)
    
