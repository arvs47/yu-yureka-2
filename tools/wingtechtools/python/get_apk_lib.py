#!/usr/bin/env python
# author: huyonggang 20140905

import os
import commands
import sys

APK_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk"
APKLIB_TEMP_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/lib_tmp"
APKLIB64_TEMP_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/lib64_tmp"
APKLIB_TEMP_DIR2 = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/tmpdir"
ARMEABI_V8A_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/tmpdir/lib/arm64-v8a"
ARMEABI_V7A_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/tmpdir/lib/armeabi-v7a"
ARMEABI_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk/tmpdir/lib/armeabi"

os.system("rm -rf %s"%APKLIB_TEMP_DIR)
os.system("rm -rf %s"%APKLIB64_TEMP_DIR)
os.system("mkdir %s"%APKLIB_TEMP_DIR)
os.system("mkdir %s"%APKLIB64_TEMP_DIR)

cmd = "find %s -name '*.apk'"%APK_DIR
apklist = commands.getstatusoutput(cmd)[1].split()

board_mode = sys.argv[1]

print "board_mode = %s"%board_mode

file_3rdlib_lines = ["PRODUCT_COPY_FILES += \\\n"]
flag_32_64 = 32

file_apk_module_lines = ["PRODUCT_PACKAGES += \\\n"]

def get_apk_module_name(apk_path):
    return os.path.basename(apk_path)[:-4]

def generate_module_lib_list(apk_path, lib_direct):
    local_module_name = get_apk_module_name(apk_path)
    print "local_module_name = %s"%local_module_name
    cmd_list_lib = "ls -1 %s"%lib_direct
    liblist = commands.getstatusoutput(cmd_list_lib)[1].split()
    for so_name in liblist:
        if "priv-app" in apk_path:
            file_3rdlib_lines.append("%s/%s:system/priv-app/%s/lib/arm/%s \\\n"%(APKLIB_TEMP_DIR,so_name,local_module_name,so_name))
        elif "operator/app" in apk_path:
            file_3rdlib_lines.append("%s/%s:system/vendor/operator/app/%s/lib/arm/%s \\\n"%(APKLIB_TEMP_DIR,so_name,local_module_name,so_name))
        elif "app" in apk_path:
            file_3rdlib_lines.append("%s/%s:system/vendor/app/%s/lib/arm/%s \\\n"%(APKLIB_TEMP_DIR,so_name,local_module_name,so_name))

for one in apklist:
    os.system("mkdir %s"%APKLIB_TEMP_DIR2)
    os.system("unzip %s -d %s"%(one,APKLIB_TEMP_DIR2))
    tmp_module_name = get_apk_module_name(one)
    file_apk_module_lines.append("%s \\\n"%tmp_module_name)
    # get 64bit lib 
    if "64" in board_mode:
        flag_32_64 = 64
        if os.path.exists(ARMEABI_V8A_DIR):
            os.system("cp %s/*.so %s/"%(ARMEABI_V8A_DIR,APKLIB64_TEMP_DIR))
        elif os.path.exists(ARMEABI_V7A_DIR):
            os.system("cp %s/*.so %s/"%(ARMEABI_V7A_DIR,APKLIB_TEMP_DIR))
            generate_module_lib_list(one, ARMEABI_V7A_DIR)
        elif os.path.exists(ARMEABI_DIR):
            os.system("cp %s/*.so %s/"%(ARMEABI_DIR,APKLIB_TEMP_DIR))
            generate_module_lib_list(one, ARMEABI_DIR)
        else:
            print "!Caption: %s without lib"%one
    else:
        if os.path.exists(ARMEABI_V7A_DIR):
            os.system("cp %s/*.so %s/"%(ARMEABI_V7A_DIR,APKLIB_TEMP_DIR))
        elif os.path.exists(ARMEABI_DIR):
            os.system("cp %s/*.so %s/"%(ARMEABI_DIR,APKLIB_TEMP_DIR))
        else:
            print "!Caption: %s without lib"%one
    os.system("rm -rf %s"%APKLIB_TEMP_DIR2)

if flag_32_64 == 32:
    cmd32 = "ls -1 %s"%APKLIB_TEMP_DIR
    liblist32 = commands.getstatusoutput(cmd32)[1].split()
    lines32 = ["PRODUCT_COPY_FILES += \\\n"]
    for one32 in liblist32:
        lines32.append("%s/%s:system/lib/%s \\\n"%(APKLIB_TEMP_DIR,one32,one32))
    lines32[-1] = lines32[-1][:-2]

    f = open("%s/3rdapk_lib.mk"%APK_DIR,"wb+")
    for one32_ex in lines32:
        f.write(one32_ex)    
    f.close()
else:
    file_3rdlib_lines[-1] = file_3rdlib_lines[-1][:-2]
    f = open("%s/3rdapk_lib.mk"%APK_DIR,"wb+")
    for one in file_3rdlib_lines:
        f.write(one)
    f.close()

cmd3 = "ls -1 %s"%APKLIB64_TEMP_DIR
liblist = commands.getstatusoutput(cmd3)[1].split()
lines = ["PRODUCT_COPY_FILES += \\\n"]
for one in liblist:
    lines.append("%s/%s:system/lib64/%s \\\n"%(APKLIB64_TEMP_DIR,one,one))

lines[-1] = lines[-1][:-2]

f = open("%s/3rdapk_lib64.mk"%APK_DIR,"wb+")
for one in lines:
    f.write(one)
f.close()

file_apk_module_lines[-1] = file_apk_module_lines[-1][:-2]
f = open("%s/3rdapk_module_name.mk"%APK_DIR,"wb+")
for one in file_apk_module_lines:
    f.write(one)
f.close()
