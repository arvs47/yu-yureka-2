#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import time
import commands
import getopt


os.chdir("android")

if not os.path.exists("wingcust"):
    print "No project found!"
    sys.exit(-1)
lists = commands.getstatusoutput("ls -1 --ignore build wingcust")[1].split("\n")
if len(lists) == 0:
    print "No project found!"
    sys.exit(-1)
for one in lists:
    pl = "Project: %s"%one
    listone = commands.getstatusoutput("ls -1 wingcust/%s"%one)[1].split("\n")
    if "base" in listone:
        listone.remove("base")
    if len(listone) > 0:
        pl = pl + "    Custom:"
        for item in listone:
            pl = pl + " %s "%item
    print pl
            
