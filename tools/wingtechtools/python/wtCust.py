#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import time
import commands
import getopt

DEVICE_DIR = "device/qcom"
LK_DIR = "bootable/bootloader/lk/target"
VENDOR_DIR = "vendor/qcom/proprietary/common"
PRODUCT_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product"
FASTMMI_DIR = "vendor/qcom/proprietary/fastmmi/mmi"
GMS_DIR = "vendor/google"
QMSSBIN_DIR = "vendor/wingtech/build/tools/binary"
TRDAPK_DIR = "vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk"
BOOTANIMATION_DIR = "vendor/qcom/proprietary/qrdplus/Extension/apps/BootAnimation"
BOOTANIMATION_CMCC_DIR = "vendor/qcom/proprietary/qrdplus/InternalUseOnly/Carrier/Resource/BootAnimation/cmcc"
BOOTANIMATION_CU_DIR = "vendor/qcom/proprietary/qrdplus/InternalUseOnly/Carrier/Resource/BootAnimation/cu"
BOOTANIMATION_CT_DIR = "vendor/qcom/proprietary/qrdplus/InternalUseOnly/Carrier/Resource/BootAnimation/ct"
QMSSBIN_DIR = "vendor/wingtech/build/tools/binary"


def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]

def wt_sys(cmd):
    os.system("%s >/dev/null 2>&1"%cmd)

if __name__ == '__main__':
    project = sys.argv[1]
    if project.endswith("_32") or project.endswith("_64") or project.endswith("_512"):
        father = project
        son = ""
    elif "_32" in project or "_64" in project or "_512" in project:
        son = project.split("_")[-1]
        father = project.split(son)[0][:-1]
    elif "_tra" in project:
        father = project.split("_")[0]+'_tra'
        try:
            son = project.split("_tra_")[1]
        except Exception:
            son = ""
    else:
        father = project.split("_")[0]
        try:
            son = project.split("_")[1]
        except Exception:
            son = ""
    # +Add, zhouchao.wt,20170405, 3rdapk process
    if os.path.exists("vendor/3rdapk"):
        wt_sys("rm -rf vendor/3rdapk")
    # -Add, zhouchao.wt,20170405, 3rdapk process
    if not os.path.exists("wingcust/%s"%father):
        sys.exit(-1)
    if father == "ido" and father != project:
        wt_sys("rm -rf device/qcom/tmp;mkdir -p device/qcom/tmp")
        wt_sys("cp device/qcom/%s/* device/qcom/tmp/. -af"%project)
        wt_sys("cp device/qcom/ido/* device/qcom/%s/. -af"%project)
        wt_sys("cp device/qcom/tmp/* device/qcom/%s/. -af"%project)
        wt_sys("rm -rf device/qcom/tmp")
    if father == "jack" and father != project:
        wt_sys("rm -rf device/qcom/tmp;mkdir -p device/qcom/tmp")
        wt_sys("cp device/qcom/%s/* device/qcom/tmp/. -af"%project)
        wt_sys("cp device/qcom/jack/* device/qcom/%s/. -af"%project)
        wt_sys("cp device/qcom/tmp/* device/qcom/%s/. -af"%project)
        wt_sys("rm -rf device/qcom/tmp")
    if father == "land" and father != project:
        wt_sys("rm -rf device/qcom/tmp;mkdir -p device/qcom/tmp")
        wt_sys("cp device/qcom/%s/* device/qcom/tmp/. -af"%project)
        wt_sys("cp device/qcom/land/* device/qcom/%s/. -af"%project)
        wt_sys("cp device/qcom/tmp/* device/qcom/%s/. -af"%project)
        wt_sys("rm -rf device/qcom/tmp")
    if father == "wt88047" and father != project:
        wt_sys("rm -rf device/qcom/tmp;mkdir -p device/qcom/tmp")
        wt_sys("cp device/qcom/%s/* device/qcom/tmp/. -af"%project)
        wt_sys("cp device/qcom/wt88047/* device/qcom/%s/. -af"%project)
        wt_sys("cp device/qcom/tmp/* device/qcom/%s/. -af"%project)
        wt_sys("rm -rf device/qcom/tmp")
    if os.path.exists("wingcust/%s/base"%father):
        if os.path.exists("wingcust/%s/base/device"%father) and father != "ido" and father != "jack" and father != "wt88047" and father != "land":
            wt_sys("rm -rf %s/%s "%(DEVICE_DIR,project))
            wt_sys("cp wingcust/%s/base/device %s/%s -af"%(father,DEVICE_DIR,project))
        if os.path.exists("wingcust/%s/base/lk"%father):
            wt_sys("rm -rf %s/%s "%(LK_DIR,project))
            wt_sys("cp wingcust/%s/base/device %s/%s -af"%(father,LK_DIR,project))
        if os.path.exists("wingcust/%s/base/vendor/BoardConfigVendor.mk"%father):
            wt_sys("rm -rf %s/%s "%(VENDOR_DIR,project))
            wt_sys("mkdir -p %s/%s"%(VENDOR_DIR,project))
            wt_sys("cp wingcust/%s/base/vendor/BoardConfigVendor.mk %s/%s/. -af"%(father,VENDOR_DIR,project))
        if os.path.exists("wingcust/%s/base/vendor/qmss_bin"%father):
            wt_sys("cp wingcust/%s/base/vendor/qmss_bin/* %s/."%(father,QMSSBIN_DIR))
        if os.path.exists("wingcust/%s/base/vendor/qcom/proprietary/fastmmi/mmi"%father):
            wt_sys("cp wingcust/%s/base/vendor/qcom/proprietary/fastmmi/mmi/* %s -af"%(father,FASTMMI_DIR))
        if os.path.exists("wingcust/%s/base/google"%father):
            wt_sys("cp wingcust/%s/base/google/* %s/. -af"%(father,GMS_DIR))
        if os.path.exists("wingcust/%s/base/wt_overlay"%father):
            wt_sys("cp wingcust/%s/base/wt_overlay/* . -af"%father)
    if not son == "":
        if os.path.exists("wingcust/%s/%s/device"%(father,son)) and father != "ido" and father != "jack" and father != "wt88047" and father != "land":
            wt_sys("cp wingcust/%s/%s/device/* %s/%s/. -af"%(father,son,DEVICE_DIR,project))
            if os.path.isfile("wingcust/%s/%s/device/ProjectConfig.mk"%(father,son)) and os.path.isfile("wingcust/%s/base/device/ProjectConfig.mk"%father) :
                os.system("python tools/wingtechtools/build/tools/config/merge-project.py wingcust/%s/base/device/ProjectConfig.mk wingcust/%s/%s/device/ProjectConfig.mk > %s/%s/ProjectConfig.mk "%(father,father,son,DEVICE_DIR,project))
        if os.path.exists("wingcust/%s/%s/lk"%(father,son)):
            wt_sys("cp wingcust/%s/%s/lk/* %s/%s/. -af"%(father,son,LK_DIR,project))
        if os.path.exists("wingcust/%s/%s/vendor/BoardConfigVendor.mk"%(father,son)):
            wt_sys("cp wingcust/%s/%s/vendor/BoardConfigVendor.mk %s/%s/. -af"%(father,son,VENDOR_DIR,project))
        if os.path.exists("wingcust/%s/%s/vendor/qmss_bin"%(father,son)):
            wt_sys("cp wingcust/%s/%s/vendor/qmss_bin/* %s/."%(father,son,QMSSBIN_DIR))
        if os.path.exists("wingcust/%s/%s/vendor/qcom/proprietary/fastmmi/mmi"%(father,son)):
            wt_sys("cp wingcust/%s/%s/vendor/qcom/proprietary/fastmmi/mmi/* %s -af"%(father,son,FASTMMI_DIR))
        if os.path.exists("wingcust/%s/%s/google"%(father,son)):
            wt_sys("cp wingcust/%s/%s/google/* %s/. -af"%(father,son,GMS_DIR))
        if os.path.exists("wingcust/%s/%s/wt_overlay"%(father,son)):
            wt_sys("cp wingcust/%s/%s/wt_overlay/* . -af"%(father,son))

    if os.path.exists("device/qcom/%s/BootAnimation"%project):
        wt_sys("cp device/qcom/%s/BootAnimation/* %s"%(project,BOOTANIMATION_DIR))
    if os.path.exists("device/qcom/%s/BootAnimation/CMCC"%project):
        if not os.path.exists(BOOTANIMATION_CMCC_DIR):
            wt_sys("mkdir -p %s"%BOOTANIMATION_CMCC_DIR)
        wt_sys("cp device/qcom/%s/BootAnimation/CMCC/* %s"%(project,BOOTANIMATION_CMCC_DIR))
    if os.path.exists("device/qcom/%s/BootAnimation/CU"%project):
        if not os.path.exists(BOOTANIMATION_CU_DIR):
            wt_sys("mkdir -p %s"%BOOTANIMATION_CU_DIR)
        wt_sys("cp device/qcom/%s/BootAnimation/CU/* %s"%(project,BOOTANIMATION_CU_DIR))
    if os.path.exists("device/qcom/%s/BootAnimation/CT"%project):
        if not os.path.exists(BOOTANIMATION_CT_DIR):
            wt_sys("mkdir -p %s"%BOOTANIMATION_CT_DIR)
        wt_sys("cp device/qcom/%s/BootAnimation/CT/* %s"%(project,BOOTANIMATION_CT_DIR))

    filename = "%s/%s/BoardConfig.mk"%(DEVICE_DIR,project)
    try:
        board_mode = wt_get("grep -irn TARGET_BOARD_SUFFIX %s"%filename).split("_")[-1]
    except Exception:
        print "Define TARGET_BOARD_SUFFIX error, please check BoardConfig.mk"
        sys.exit(-1)
    wt_sys("rm -rf vendor/qcom/proprietary/prebuilt_HY11/target/product/common/system/vendor/3rdapk")
    if not son == "":
        TARGET_BOARD_PLATFORM = wt_get("cat wingcust/%s/%s/device/BoardConfig.mk | grep TARGET_BOARD_PLATFORM | sed 's/ //g'|sed 's/TARGET_BOARD_PLATFORM:=//g' "%(father,son))
    else:
        TARGET_BOARD_PLATFORM = wt_get("cat wingcust/%s/base/device/BoardConfig.mk | grep TARGET_BOARD_PLATFORM | sed 's/ //g'|sed 's/TARGET_BOARD_PLATFORM:=//g' "%father)
    if "64" in board_mode:
        wt_sys("rm -rf vendor/qcom/proprietary/prebuilt_HY11/target/product/common")
        wt_sys("ln -s %s_64 vendor/qcom/proprietary/prebuilt_HY11/target/product/common"%TARGET_BOARD_PLATFORM)
    if "32" in board_mode:
        wt_sys("rm -rf vendor/qcom/proprietary/prebuilt_HY11/target/product/common")
        wt_sys("ln -s %s_32 vendor/qcom/proprietary/prebuilt_HY11/target/product/common"%TARGET_BOARD_PLATFORM)


    if os.path.exists("wingcust/%s/base"%father):
        if os.path.exists("wingcust/%s/base/product"%father):
            wt_sys("cp wingcust/%s/base/product/* %s/common/. -af"%(father,PRODUCT_DIR))
    if not son == "":
        if os.path.exists("wingcust/%s/%s/product"%(father,son)):
            wt_sys("cp wingcust/%s/%s/product/* %s/common/. -af"%(father,son,PRODUCT_DIR))

    if os.path.exists("device/qcom/%s/Logo"%project):
        if project.startswith("wt88047") or project.startswith("wt86047"):
            pass
        else:            
            wt_sys("python tools/wingtechtools/python/logo_gen.py device/qcom/%s/Logo/logo.png"%project)
            wt_sys("mv splash.img splash_logo.img")
            if not os.path.exists("device/qcom/%s/Logo/fastboot.png"%project):
                wt_sys("cp device/qcom/%s/Logo/logo.png device/qcom/%s/Logo/fastboot.png"%(project,project))
            if os.path.exists("device/qcom/%s/Logo/fastboot.png"%project):
                wt_sys("python tools/wingtechtools/python/logo_gen.py device/qcom/%s/Logo/fastboot.png"%project)
                wt_sys("mv splash.img splash_fastboot.img")
                os.system("cat splash_fastboot.img >> splash_logo.img")
            wt_sys("mv splash_logo.img splash.img")
            wt_sys("mv splash.img %s/."%QMSSBIN_DIR)

    if father == "ido" and father != project:
        wt_sys("cp device/qcom/ido/splash.img %s/."%QMSSBIN_DIR)
    elif father == "jack" and father != project:
        wt_sys("cp device/qcom/jack/splash.img %s/."%QMSSBIN_DIR)
    elif father == "wt88047" and father != project:
        wt_sys("cp device/qcom/wt88047/splash.img %s/."%QMSSBIN_DIR)
    elif father == "land" and father != project:
        wt_sys("cp device/qcom/land/splash.img %s/."%QMSSBIN_DIR)
        
    wt_sys("cp %s/NON-HLOS.bin device/qcom/%s/radio/."%(QMSSBIN_DIR,project))
    wt_sys("cp %s/sbl1.mbn device/qcom/%s/radio/."%(QMSSBIN_DIR,project))
    wt_sys("cp %s/tz.mbn device/qcom/%s/radio/."%(QMSSBIN_DIR,project))
    wt_sys("cp %s/rpm.mbn device/qcom/%s/radio/."%(QMSSBIN_DIR,project))
    wt_sys("cp %s/hyp.mbn device/qcom/%s/radio/."%(QMSSBIN_DIR,project))

    #for cta project
    CTA_FLAG = False
    if os.path.exists("device/qcom/%s/ProjectConfig.mk"%project):
        f = open("device/qcom/%s/ProjectConfig.mk"%project)
        lines = f.readlines()
        for one in lines:
            if not one.startswith("#") and "WT_CTA_SUPPORT" in one:
                if "yes" in one:
                    CTA_FLAG = True
    if CTA_FLAG == True:
        if os.path.exists("vendor/qcom/proprietary/mm-audio_CTA"):
            wt_sys("cp -af vendor/qcom/proprietary/mm-audio_CTA/* vendor/qcom/proprietary/mm-audio")

    ################################################################
    ##################for enjoyrom##################################
    if not os.path.exists(TRDAPK_DIR):
        wt_sys("mkdir -p %s"%TRDAPK_DIR)

    if os.path.exists("enjoyrom/build/3rdapk/app"):
        if not os.path.exists("%s/app"%TRDAPK_DIR):
            wt_sys("mkdir -p %s/app"%TRDAPK_DIR)
        wt_sys("cp enjoyrom/build/3rdapk/app/*.apk %s/app/"%TRDAPK_DIR)

    if os.path.exists("enjoyrom/build/3rdapk/operator/app"):
        if not os.path.exists("%s/operator/app"%TRDAPK_DIR):
            wt_sys("mkdir -p %s/operator/app"%TRDAPK_DIR)
        wt_sys("cp -a enjoyrom/build/3rdapk/operator/app/*.apk %s/operator/app/"%TRDAPK_DIR)

    if os.path.exists("enjoyrom/build/3rdapk/priv-app"):
        if not os.path.exists("%s/priv-app"%TRDAPK_DIR):
            wt_sys("mkdir -p %s/priv-app"%TRDAPK_DIR)
        wt_sys("cp enjoyrom/build/3rdapk/priv-app/*.apk %s/priv-app/"%TRDAPK_DIR)

    filename = "%s/%s/ProjectConfig.mk"%(DEVICE_DIR,project)
    if  os.path.exists(filename):
        mode = wt_get("grep -irn WT_ENJOYROM_UI_STYLE %s"%filename).split("=")[-1]
    else:
        mode = ""
    if mode != "":
        if mode[0] == " ":
            mode = mode[1:]
    if mode != "":
        wt_sys("cp enjoyrom/romui/%s/* . -af"%mode)

    ################################################################
    ################################################################
        

    if os.path.exists("%s"%TRDAPK_DIR):
        print "Begin extract 3rdapk lib..."
        wt_sys("python tools/wingtechtools/python/get_apk_lib.py %s"%board_mode)
        print "End extract 3rdapk lib."
