#!/usr/bin/env python
import os
import sys
import time
import xlrd
import xlwt
import commands
import getpass
import re
import xmlrpclib
sys.path.insert(1,'./android/tools/wingtechtools/python/')


def usage():
    print "useage:"
    print "        ./wt bugstatus buglistfile"
    print "            buglistfile mustbe bugs num only , emample"
    print "                "
    print "                142131"
    print "                142138"
    sys.exit(-1)
def get_branch():
    branch_list = []
    cmd = "cd android/frameworks/base;git branch -a;cd -"
    out = commands.getstatusoutput(cmd)[1]
    for one in out.split():
        if one.startswith("remotes/aosp/"):
            branch_list.append(one[13:])
        if one.startswith("aosp/"):
            current_branch = one[5:]
    br = raw_input("Remote branch        [%s/?]: "%current_branch)
    if br == "":
        br=current_branch
    elif br == "?":
        print "The remote branch list as:"
        for one in branch_list:
            print "\t"+one
        return get_branch()
    else:
        if br not in branch_list:
            print "Sorry, try again!"
            return get_branch()
    return br
     
def get_bugzilla_account():
    try:
        f = open("/home/%s/.autofixbugconfig"%USER)
        lines=f.readlines()
        for one in lines:
            one = one.strip()
            if "bugzilla_account=" in one:
                account = one.split("=")[1]
            if "bugzilla_passwd=" in one:
                passwd = one.split("=")[1]
    except Exception:
        account = ""
        passwd = ""
    return account,passwd
   
def get_email():
    default_email = "%s@wingtech.net"%USER
    t1 = raw_input("Bugzilla Account       [%s]: "%default_email)
    if t1 == "":
        t1 = default_email
    return t1

def get_passwd():
    t1  = getpass.getpass('password: ')
    if t1 == "exit":sys.exit(-1)
    try:
        server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
        dl = server.User.login({"login":"%s"%bugzilla_account,"password":"%s"%t1})
    except:
        print "Sorry, try again!"
        return get_passwd()
    return t1

def onremember_password():
    t1 = raw_input("Remember the password          [Y/n]: ")
    if t1 == "":
        t1 = "Y"
    if t1 == "Y":
        f=open("/home/%s/.autofixbugconfig"%USER,"w")
        f.write("bugzilla_account=%s\n"%bugzilla_account)
        f.write("bugzilla_passwd=%s"%bugzilla_passwd)
        f.close()
def generate(file):
    lists = []

    f = open(file)
    lines = f.readlines()
    for i in lines:
        if len(i.strip()) > 0:
            lists.append(i.strip())
    print lists
    lists.sort()
    total_number = len(lists)
    server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
    dl = server.User.login({"login":bugzilla_account,"password":bugzilla_passwd})
    tk = dl.values()[0]
    print "\n......\n"
    print "A total of %d records, begin to check the status on bugzilla!"%total_number
    lists2 = []
    i = 1
    for one in lists:
        try:
            ori_status = server.Bug.get({"ids":one,'token':tk}).values()[1][0].values()[-3]
        except Exception:
            ori_status = ""
        print "[%d / %d] %s : %s"%(i,total_number,one,ori_status)
    
def autofix(lists):
    server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
    dl = server.User.login({"login":bugzilla_account,"password":bugzilla_passwd})
    tk = dl.values()[0]
    t1 = raw_input("Auto fix on bugzilla        [Y/n]: ")
    if t1 == "":
        t1 = "Y"
    if t1 == "Y":
        comment = raw_input("Additional Comments       []: ")
        if comment == "":
            print "You should enter at least one world as the addtional comments, such as 'for fix version:V011'!"
            comment = raw_input("Additional Comments       []: ")
        if comment == "":
            print "You should enter at least one world as the addtional comments, such as 'for fix version:V011'!"
            comment = raw_input("Additional Comments       []: ")
        print "begin to auto fix......"
        for one in lists:
            server.Bug.update({'ids':one,'comment':{'body':comment},'resolution':'FIXED','token':tk})
            print "Bug %s : from CODECOMM to FIXED successed!"%one
    else:
        sys.exit(-1)

if __name__ == '__main__':
    if not len(sys.argv) == 2:
        usage()
    file=sys.argv[1].replace('[','').replace(']','')
    if not os.path.isfile(file):
        print "not exists bug list file : %s "%file
        usage()
    
    USER = getpass.getuser()
    print "*** check bugs status : %s"%file
    print "***\n"
    if get_bugzilla_account()[0] == "" or get_bugzilla_account()[1] == "":
        print "\n*** Bugzilla Account"
        print "***\n"
        bugzilla_account = get_email()
        bugzilla_passwd = get_passwd()
        onremember_password()
    else:
        bugzilla_account = get_bugzilla_account()[0]
        bugzilla_passwd = get_bugzilla_account()[1]    
    generate(file)


