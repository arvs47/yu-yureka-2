#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import commands
import sys

def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]
    
def usage():
    print "usage:  ./change_str1_to_str2.py config_file src_file bak_file"
    print "config_file must contain lines like boot.img:B3I100EUOD000.MBN"
    sys.exit(-1)
    
if __name__ == '__main__':
    if not len(sys.argv) ==4:
        usage()
    config_file=sys.argv[1]
    src_file=sys.argv[2]
    bak_file=sys.argv[3]
   
    if os.path.isfile(config_file) and os.path.isfile(src_file):
        pass
    else:
        print "not exist file \n   %s\nor %s \n please check !!! "%(config_file,src_file)
        sys.exit(-1)
    if len(os.path.dirname(bak_file)) > 0:
        os.system("mkdir -p %s"%(os.path.dirname(bak_file)))
    os.system("cp %s %s"%(src_file,bak_file))
    f=open(config_file,'r')
    unparse_flag=0
    cmd_unparse=''
    for line in f: 
        if ':' in line and not line.strip().startswith("#") and not line.strip().startswith("//"):
            cmd = 'sed -i "s/%s/%s/" %s'%(line.strip().split(':')[0],line.strip().split(':')[1],bak_file)
            os.system(cmd)
            if os.path.isfile(line.strip().split(':')[0]):
                if 'rawprogram_unsparse.xml' == line.strip().split(':')[0] :
                    unparse_flag=1
                    rawprogram_unsparse_name=line.strip().split(':')[1]
                    cmd_unparse = 'mv %s %s '%(bak_file,line.strip().split(':')[1])
                elif 'rawprogram_upgrade.xml' == line.strip().split(':')[0] or 'patch0.xml' == line.strip().split(':')[0] :
                    cmd = 'cp %s  %s'%(line.strip().split(':')[0],line.strip().split(':')[1])
                else:
                    cmd = 'mv %s  %s'%(line.strip().split(':')[0],line.strip().split(':')[1])
                os.system(cmd)
                if "boot.img" == line.strip().split(':')[0] :
                    boot_name = line.strip().split(':')[1]
    dsfile="ds.ini"
    os.system("echo \"[Download]\" >%s"%dsfile)
    os.system("echo \"eMMC Programmer=prog_emmc_firehose_8929.mbn\" >>%s"%dsfile)
    os.system("echo \"Raw Program=%s\" >>%s"%(rawprogram_unsparse_name,dsfile))
    os.system("echo \"Patch0=patch0.xml\" >>%s"%dsfile)
    os.system("echo \"Boot=%s\" >>%s"%(boot_name,dsfile))
    if unparse_flag==1:
       os.system(cmd_unparse)
    f.close()
    
