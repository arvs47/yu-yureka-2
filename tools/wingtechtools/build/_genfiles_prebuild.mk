hide  :=  @

WINGTECH_ROOT := $(TOPDIR)tools/wingtechtools
PRJ_MF      := $(TOPDIR)tools/wingtechtools/build/PRE_GEN/ProjectConfig.mk
JAVAOPTION_PM := $(TOPDIR)device/qcom/common/javaoption.pm

JAVAOPTFILEPATH := $(TOPDIR)frameworks/base/core/java/com/mediatek/common/featureoption
JAVAOPTFILE := $(JAVAOPTFILEPATH)/FeatureOption.java

GEN_ALL_FILES := $(JAVAOPTFILE) 
WINGTECH_PROJECT_CONFIGS := $(TOPDIR)device/qcom/common/ProjectConfig.mk $(TOPDIR)device/qcom/$(TARGET_PRODUCT)/ProjectConfig.mk 


.PHONY : all
all: genfiles_prebuild
genfiles_prebuild:  $(GEN_ALL_FILES)
$(JAVAOPTFILE): merge_config_files $(WINGTECH_ROOT)/build/tools/javaoptgen.pl $(PRJ_MF) $(JAVAOPTION_PM)
	$(hide) -rm -rf $(JAVAOPTFILE)
	$(hide) perl $(WINGTECH_ROOT)/build/tools/javaoptgen.pl $(PRJ_MF) $(JAVAOPTFILE) $(JAVAOPTION_PM)
merge_config_files:$(PRJ_MF)
$(PRJ_MF):$(WINGTECH_ROOT)/build/tools/config/merge-project.py $(WINGTECH_PROJECT_CONFIGS)
	$(hide) -rm -rf $(PRJ_MF)
	$(hide) mkdir -p $(dir $@)
	python $(WINGTECH_ROOT)/build/tools/config/merge-project.py $(WINGTECH_PROJECT_CONFIGS) > $@
